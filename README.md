# Steps to execute this project

1. Import Project as Maven in eclipse IDE

2. Execute below maven commands
  mvn clean
  mvn install
  eclipse:eclise
  
 3. Add all the libs to the Build Project
 
 4. Execute InsuranceQuoteGeneratorApplication.java file as Spring Boot Application
 
 5. Use any below tool to trigger the request 
 
   1. POSTMAN
   2. REST ADVANCED CLIENT
   3. SOAP UI
   
  6. Request URL 
  http://localhost:8080/getHealthInsurancePremiumQuote
  
  Method Type : POST
  
  Application Type : application/json
  
  Sample Request Payload:
  ------------------------
  {
    "id": 101,
    "firstName": "Norman",
    "lastName": "Gomes",
    "age": "34",
    "gender": "M",
    "hasHypertension": false,
    "hasBP": false,
    "hasBloodSugar": false,
    "overWeight": true,
    "smoker": false,
    "alcholic": true,
    "excerciseFlag": true,
    "drugAddict": false
}
