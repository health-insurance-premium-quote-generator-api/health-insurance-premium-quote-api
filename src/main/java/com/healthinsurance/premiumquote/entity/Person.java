package com.healthinsurance.premiumquote.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Person.
 */
@Entity
@Table(name = "person")
public class Person {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The age. */
	private int age;

	/** The gender. */
	private char gender;

	/** The has hypertension. */
	private boolean hasHypertension;

	/** The has BP. */
	private boolean hasBP;

	/** The has blood sugar. */
	private boolean hasBloodSugar;

	/** The is over weight. */
	private boolean isOverWeight;

	/** The is smoker. */
	private boolean isSmoker;

	/** The is alcholic. */
	private boolean isAlcholic;

	/** The excercise flag. */
	private boolean excerciseFlag;

	/**
	 * Instantiates a new person.
	 */
	public Person() {
	}

	/** The is drug addict. */
	private boolean isDrugAddict;

	/**
	 * Instantiates a new person.
	 *
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param age
	 *            the age
	 * @param gender
	 *            the gender
	 * @param hasHypertension
	 *            the has hypertension
	 * @param hasBP
	 *            the has BP
	 * @param hasBloodSugar
	 *            the has blood sugar
	 * @param isOverWeight
	 *            the is over weight
	 * @param isSmoker
	 *            the is smoker
	 * @param isAlcholic
	 *            the is alcholic
	 * @param excerciseFlag
	 *            the excercise flag
	 * @param isDrugAddict
	 *            the is drug addict
	 */
	public Person(String firstName, String lastName, int age, char gender, boolean hasHypertension, boolean hasBP,
			boolean hasBloodSugar, boolean isOverWeight, boolean isSmoker, boolean isAlcholic, boolean excerciseFlag,
			boolean isDrugAddict) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.hasHypertension = hasHypertension;
		this.hasBP = hasBP;
		this.hasBloodSugar = hasBloodSugar;
		this.isOverWeight = isOverWeight;
		this.isSmoker = isSmoker;
		this.isAlcholic = isAlcholic;
		this.excerciseFlag = excerciseFlag;
		this.isDrugAddict = isDrugAddict;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the frist name.
	 *
	 * @param firstName the new frist name
	 */
	public void setFristName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the age.
	 *
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Sets the age.
	 *
	 * @param age
	 *            the new age
	 */
	public void setAge(final int age) {
		this.age = age;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public char getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender
	 *            the new gender
	 */
	public void setGender(final char gender) {
		this.gender = gender;
	}

	/**
	 * Checks if is checks for hypertension.
	 *
	 * @return true, if is checks for hypertension
	 */
	public boolean isHasHypertension() {
		return hasHypertension;
	}

	/**
	 * Sets the checks for hypertension.
	 *
	 * @param hasHypertension
	 *            the new checks for hypertension
	 */
	public void setHasHypertension(final boolean hasHypertension) {
		this.hasHypertension = hasHypertension;
	}

	/**
	 * Checks if is checks for BP.
	 *
	 * @return true, if is checks for BP
	 */
	public boolean isHasBP() {
		return hasBP;
	}

	/**
	 * Sets the checks for BP.
	 *
	 * @param hasBP
	 *            the new checks for BP
	 */
	public void setHasBP(final boolean hasBP) {
		this.hasBP = hasBP;
	}

	/**
	 * Checks if is checks for blood sugar.
	 *
	 * @return true, if is checks for blood sugar
	 */
	public boolean isHasBloodSugar() {
		return hasBloodSugar;
	}

	/**
	 * Sets the checks for blood sugar.
	 *
	 * @param hasBloodSugar
	 *            the new checks for blood sugar
	 */
	public void setHasBloodSugar(final boolean hasBloodSugar) {
		this.hasBloodSugar = hasBloodSugar;
	}

	/**
	 * Checks if is over weight.
	 *
	 * @return true, if is over weight
	 */
	public boolean isOverWeight() {
		return isOverWeight;
	}

	/**
	 * Sets the over weight.
	 *
	 * @param overWeight
	 *            the new over weight
	 */
	public void setOverWeight(final boolean overWeight) {
		isOverWeight = overWeight;
	}

	/**
	 * Checks if is smoker.
	 *
	 * @return true, if is smoker
	 */
	public boolean isSmoker() {
		return isSmoker;
	}

	/**
	 * Sets the smoker.
	 *
	 * @param smoker
	 *            the new smoker
	 */
	public void setSmoker(final boolean smoker) {
		isSmoker = smoker;
	}

	/**
	 * Checks if is alcholic.
	 *
	 * @return true, if is alcholic
	 */
	public boolean isAlcholic() {
		return isAlcholic;
	}

	/**
	 * Sets the alcholic.
	 *
	 * @param alcholic
	 *            the new alcholic
	 */
	public void setAlcholic(final boolean alcholic) {
		isAlcholic = alcholic;
	}

	/**
	 * Checks if is excercise flag.
	 *
	 * @return true, if is excercise flag
	 */
	public boolean isExcerciseFlag() {
		return excerciseFlag;
	}

	/**
	 * Sets the excercise flag.
	 *
	 * @param excerciseFlag
	 *            the new excercise flag
	 */
	public void setExcerciseFlag(final boolean excerciseFlag) {
		this.excerciseFlag = excerciseFlag;
	}

	/**
	 * Checks if is drug addict.
	 *
	 * @return true, if is drug addict
	 */
	public boolean isDrugAddict() {
		return isDrugAddict;
	}

	/**
	 * Sets the drug addict.
	 *
	 * @param drugAddict
	 *            the new drug addict
	 */
	public void setDrugAddict(final boolean drugAddict) {
		isDrugAddict = drugAddict;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Person{" + "id=" + id + ", fullName='" + firstName + '\'' + ", age=" + age + ", gender=" + gender
				+ ", hasHypertension=" + hasHypertension + ", hasBP=" + hasBP + ", hasBloodSugar=" + hasBloodSugar
				+ ", isOverWeight=" + isOverWeight + ", isSmoker=" + isSmoker + ", isAlcholic=" + isAlcholic
				+ ", excerciseFlag=" + excerciseFlag + ", isDrugAddict=" + isDrugAddict + '}';
	}

}
