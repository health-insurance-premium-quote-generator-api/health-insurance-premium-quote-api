package com.healthinsurance.premiumquote.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.healthinsurance.premiumquote.entity.Person;

/**
 * The Interface QuoteRepository.
 */
public interface QuoteRepository extends JpaRepository<Person, Long> {
}
